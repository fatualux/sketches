# SKETCHES

### Some ideas for various purposes.

## USAGE

```
Feel free to use this material but please cite the author.
```

## LINKS

[Author - FZ](https://fatualux.github.io/)


## LICENSE

[![License](https://img.shields.io/badge/License-CC%20BY%20v3-blue.svg)](http://creativecommons.org/licenses/by/3.0/legalcode)

This project is licensed under the CC-BY license.
See LICENSE file for more details.
