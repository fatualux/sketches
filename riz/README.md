<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="style.css">
</head>
<body>

## AUTHOR

- [Francesco Zubani](https://fatualux.github.io/)

## AKNOWLEDGEMENTS

Only FOSS software was used to generate the gallery.

This work was made possible by the following projects:

- [GIMP](https://www.gimp.org/)
- [Inkscape](https://inkscape.org/)

## Drafts

<div class="gallery">
  <a href="ComfyUI_00004_.png"><img class="thumbnail" src="./thumbs/ComfyUI_00004_.png" alt="ComfyUI_00004_"></a>
  <a href="ComfyUI_00006_.png"><img class="thumbnail" src="./thumbs/ComfyUI_00006_.png" alt="ComfyUI_00006_"></a>
  <a href="ComfyUI_00008_.png"><img class="thumbnail" src="./thumbs/ComfyUI_00008_.png" alt="ComfyUI_00008_"></a>
  <a href="ComfyUI_00010_.png"><img class="thumbnail" src="./thumbs/ComfyUI_00010_.png" alt="ComfyUI_00010_"></a>
  <a href="ComfyUI_00012_.png"><img class="thumbnail" src="./thumbs/ComfyUI_00012_.png" alt="ComfyUI_00012_"></a>
  <a href="ComfyUI_00013_.png"><img class="thumbnail" src="./thumbs/ComfyUI_00013_.png" alt="ComfyUI_00013_"></a>
  <a href="ComfyUI_00014_.png"><img class="thumbnail" src="./thumbs/ComfyUI_00014_.png" alt="ComfyUI_00014_"></a>
  <a href="ComfyUI_00015_.png"><img class="thumbnail" src="./thumbs/ComfyUI_00015_.png" alt="ComfyUI_00015_"></a>
  <a href="ComfyUI_00020_.png"><img class="thumbnail" src="./thumbs/ComfyUI_00020_.png" alt="ComfyUI_00020_"></a>
  <a href="ComfyUI_00021_.png"><img class="thumbnail" src="./thumbs/ComfyUI_00021_.png" alt="ComfyUI_00021_"></a>
  <a href="ComfyUI_00023_.png"><img class="thumbnail" src="./thumbs/ComfyUI_00023_.png" alt="ComfyUI_00023_"></a>
  <a href="ComfyUI_00025_.png"><img class="thumbnail" src="./thumbs/ComfyUI_00025_.png" alt="ComfyUI_00025_"></a>
  <a href="ComfyUI_00027_.png"><img class="thumbnail" src="./thumbs/ComfyUI_00027_.png" alt="ComfyUI_00027_"></a>
  <a href="ComfyUI_00028_.png"><img class="thumbnail" src="./thumbs/ComfyUI_00028_.png" alt="ComfyUI_00028_"></a>
  <a href="ComfyUI_00029_.png"><img class="thumbnail" src="./thumbs/ComfyUI_00029_.png" alt="ComfyUI_00029_"></a>
  <a href="ComfyUI_00030_.png"><img class="thumbnail" src="./thumbs/ComfyUI_00030_.png" alt="ComfyUI_00030_"></a>
  <a href="ComfyUI_00031_.png"><img class="thumbnail" src="./thumbs/ComfyUI_00031_.png" alt="ComfyUI_00031_"></a>
  <a href="ComfyUI_00032_.png"><img class="thumbnail" src="./thumbs/ComfyUI_00032_.png" alt="ComfyUI_00032_"></a>
  <a href="ComfyUI_00035_.png"><img class="thumbnail" src="./thumbs/ComfyUI_00035_.png" alt="ComfyUI_00035_"></a>
  <a href="ComfyUI_00036_.png"><img class="thumbnail" src="./thumbs/ComfyUI_00036_.png" alt="ComfyUI_00036_"></a>
  <a href="gea_00002_.png"><img class="thumbnail" src="./thumbs/gea_00002_.png" alt="gea_00002_"></a>
  <a href="gea_00004_.png"><img class="thumbnail" src="./thumbs/gea_00004_.png" alt="gea_00004_"></a>
  <a href="gea_00005_.png"><img class="thumbnail" src="./thumbs/gea_00005_.png" alt="gea_00005_"></a>
  <a href="gea_00008_.png"><img class="thumbnail" src="./thumbs/gea_00008_.png" alt="gea_00008_"></a>
  <a href="gea_00010_.png"><img class="thumbnail" src="./thumbs/gea_00010_.png" alt="gea_00010_"></a>
  <a href="gea_00019_.png"><img class="thumbnail" src="./thumbs/gea_00019_.png" alt="gea_00019_"></a>
  <a href="gea_00021_.png"><img class="thumbnail" src="./thumbs/gea_00021_.png" alt="gea_00021_"></a>
  <a href="gea_00022_.png"><img class="thumbnail" src="./thumbs/gea_00022_.png" alt="gea_00022_"></a>
  <a href="gea_00025_.png"><img class="thumbnail" src="./thumbs/gea_00025_.png" alt="gea_00025_"></a>
  <a href="gea_00027_.png"><img class="thumbnail" src="./thumbs/gea_00027_.png" alt="gea_00027_"></a>
  <a href="gea_00028_.png"><img class="thumbnail" src="./thumbs/gea_00028_.png" alt="gea_00028_"></a>
  <a href="gea_00029_.png"><img class="thumbnail" src="./thumbs/gea_00029_.png" alt="gea_00029_"></a>
  <a href="gea_00030_.png"><img class="thumbnail" src="./thumbs/gea_00030_.png" alt="gea_00030_"></a>
  <a href="gea_00031_.png"><img class="thumbnail" src="./thumbs/gea_00031_.png" alt="gea_00031_"></a>
  <a href="gea_00032_.png"><img class="thumbnail" src="./thumbs/gea_00032_.png" alt="gea_00032_"></a>
  <a href="gea_00033_.png"><img class="thumbnail" src="./thumbs/gea_00033_.png" alt="gea_00033_"></a>
  <a href="gea_00035_.png"><img class="thumbnail" src="./thumbs/gea_00035_.png" alt="gea_00035_"></a>
  <a href="gea_00037_.png"><img class="thumbnail" src="./thumbs/gea_00037_.png" alt="gea_00037_"></a>
  <a href="gea_00039_.png"><img class="thumbnail" src="./thumbs/gea_00039_.png" alt="gea_00039_"></a>
  <a href="gea_00040_.png"><img class="thumbnail" src="./thumbs/gea_00040_.png" alt="gea_00040_"></a>
  <a href="gea_00041_.png"><img class="thumbnail" src="./thumbs/gea_00041_.png" alt="gea_00041_"></a>
  <a href="gea_00042_.png"><img class="thumbnail" src="./thumbs/gea_00042_.png" alt="gea_00042_"></a>
  <a href="gea_00043_.png"><img class="thumbnail" src="./thumbs/gea_00043_.png" alt="gea_00043_"></a>
  <a href="gea_00045_.png"><img class="thumbnail" src="./thumbs/gea_00045_.png" alt="gea_00045_"></a>
  <a href="gea_00046_.png"><img class="thumbnail" src="./thumbs/gea_00046_.png" alt="gea_00046_"></a>
  <a href="gea_00047_.png"><img class="thumbnail" src="./thumbs/gea_00047_.png" alt="gea_00047_"></a>
</div>
</body>
</html>
