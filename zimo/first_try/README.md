<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="style.css">
</head>
<body>

## AUTHOR

- [Francesco Zubani](https://fatualux.github.io/)

## AKNOWLEDGEMENTS

Only FOSS software was used to generate the gallery.

This work was made possible by the following projects:

- [GIMP](https://www.gimp.org/)
- [Inkscape](https://inkscape.org/)

## First try

<div class="gallery">
  <a href="disc.png"><img class="thumbnail" src="./thumbs/disc.png" alt="disc"></a>
  <a href="test.png"><img class="thumbnail" src="./thumbs/test.png" alt="test"></a>
</div>
</body>
</html>
