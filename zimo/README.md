<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="style.css">
</head>
<body>

## AUTHOR

- [Francesco Zubani](https://fatualux.github.io/)

## AKNOWLEDGEMENTS

Only FOSS software was used to generate the gallery.

This work was made possible by the following projects:

- [GIMP](https://www.gimp.org/)
- [Inkscape](https://inkscape.org/)

## Ufficio Zero - Logo

<div class="gallery">
  <a href="inverted.png"><img class="thumbnail" src="./thumbs/inverted.png" alt="inverted"></a>
  <a href="logo.png"><img class="thumbnail" src="./thumbs/logo.png" alt="logo"></a>
  <a href="red.png"><img class="thumbnail" src="./thumbs/red.png" alt="red"></a>
  <a href="texture.png"><img class="thumbnail" src="./thumbs/texture.png" alt="texture"></a>
</div>
</body>
</html>
